#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "sdk_config.h"
#include "nrf_delay.h"
#include "boards.h"

//define and include for log
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#define NRF_LOG_ENABLED 1


// variable used for storing the led list
const uint8_t leds_list[LEDS_NUMBER] = LEDS_LIST;

/**
 * @brief Function for application main entry.
 */
int main(void)
{
  // initialization functions
	NRF_LOG_INIT(NULL);
  NRF_LOG_INFO("Init");
	
  // Configure LED-pins as outputs
  LEDS_CONFIGURE(LEDS_MASK);
	
  // Toggle LEDs
  while (true)
  {
    for (int index = 0; index < LEDS_NUMBER; index++)
    {
      LEDS_INVERT(1 << leds_list[index]);

			NRF_LOG_INFO("\nLed changed %d", index);
			
      nrf_delay_ms(1000);
    }
  }
}

