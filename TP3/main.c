#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "sdk_config.h"

#include "app_error.h"
#include "app_timer.h"
#include "sdk_errors.h"
#include "nrf_drv_clock.h"
#include "nrf_delay.h"
#include "boards.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#include "bsp.h"

// for sensors
#include "hdc1000.h"
#include "lps25hb.h"
#include "battery.h"
#include "helpers.h"

// ble
#include "ble_helpers.h"

// definitions for timer values
#define APP_TIMER_PRESCALER              							0                                       	  /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          							4                                          	/**< Size of timer operation queues. */
#define CLOCK_TIMER_INTERVAL_IN_MS                    1000                                        /**< Value for the clock timer in msecs (= 1 sec). */
#define CLOCK_TIMER_INTERVAL   					 							APP_TIMER_TICKS(CLOCK_TIMER_INTERVAL_IN_MS, APP_TIMER_PRESCALER)	/**< Value for the clock timer in number of ticks (= 1 sec). */

#define MEASUREMENT_ACTIVE 
#ifdef MEASUREMENT_ACTIVE
#define MEASUREMENT_TIMER_INTERVAL_IN_MS              9000                                       /**< Value for the measurement timer in msecs (= 1 sec). */
#define MEASUREMENT_TIMER_INTERVAL			 							APP_TIMER_TICKS(MEASUREMENT_TIMER_INTERVAL_IN_MS, APP_TIMER_PRESCALER)	/**< Value for the measurement timer in number of ticks (= 1 sec). */
#endif

// App timers definition
APP_TIMER_DEF(g_clock_timer_id);  /**< Definition of the timer used for the clock */
#ifdef MEASUREMENT_ACTIVE
APP_TIMER_DEF(g_measurement_timer_id);  /**< Definition of the measurement used for the clock */
#endif

// variable used for incrementing the clock upon each timer notification
uint32_t g_clock_counter = 0;

/**@brief Function to retrieve the clock counter value in seconds
 *
 * @return clock counter value in seconds
 */
uint32_t getClockCounter(void)
{
  // we are not allowed to log anything here ! since this function
  // is called by the logging function itself
  return g_clock_counter;
}

/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
  // provide getClockCounter() as callback for getting timestamping information
  ret_code_t err_code = NRF_LOG_INIT(getClockCounter);
  APP_ERROR_CHECK(err_code);
  
  nrf_delay_ms(1000);
}

/**@brief Function for handling the clock timer.
 *
 * @details This function will be called every CLOCK_TIMER_INTERVAL
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler (not used in this case).
 */
static void clock_timer_handler(void * p_context)
{	  
  g_clock_counter = g_clock_counter + CLOCK_TIMER_INTERVAL_IN_MS / 1000;
  // NRF_LOG_INFO("Clock counter set to %d\n", g_clock_counter);
}

/**@brief Function for handling the measurement timer.
 *
 * @details This function will be called every MEASUREMENT_TIMER_INTERVAL
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler (not used in this case).
 */
#ifdef MEASUREMENT_ACTIVE
static void measurement_timer_handler(void * p_context)
{	  
  double dCurrentTemperature = hdc1000_getTemperature();
  LogMeasurement("Current temperature is", "degrees", dCurrentTemperature);

  double dCurrentHumidity = hdc1000_getHumidity();
  LogMeasurement("Current humidity is", "%", dCurrentHumidity);
  
  double dCurrentPressure = lps25hb_getPressure();
  LogMeasurement("Current pressure is", "hPa (mbar)", dCurrentPressure);
	
	// update advertising data
	advdata_update(get_battery_level(), dCurrentTemperature, dCurrentHumidity, dCurrentPressure);
}
#endif

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates application timers.
 */
static void application_timers_init(void)
{
  // Initialize the timer module
  APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

  // Create the timers
  uint32_t err_code = app_timer_create(&g_clock_timer_id, APP_TIMER_MODE_REPEATED, clock_timer_handler);
  APP_ERROR_CHECK(err_code);
  
#ifdef MEASUREMENT_ACTIVE
  err_code = app_timer_create(&g_measurement_timer_id, APP_TIMER_MODE_REPEATED, measurement_timer_handler);
  APP_ERROR_CHECK(err_code);
#endif
}

/**@brief Function for starting timers.
*/
static void application_timers_start(void)
{
  uint32_t err_code = app_timer_start(g_clock_timer_id, CLOCK_TIMER_INTERVAL, NULL);
  APP_ERROR_CHECK(err_code);
  
#ifdef MEASUREMENT_ACTIVE
  err_code = app_timer_start(g_measurement_timer_id, MEASUREMENT_TIMER_INTERVAL, NULL);
  APP_ERROR_CHECK(err_code);
#endif
}

/**@brief Function for the Power Management.
 */
static void power_manage(void)
{
  uint32_t err_code = sd_app_evt_wait();
  APP_ERROR_CHECK(err_code);
}

/**
 * Function is implemented as weak so that it can be overwritten by custom application error handler
 * when needed.
 */
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
  NRF_LOG_ERROR("Fatal\r\n");
  
  NRF_LOG_FINAL_FLUSH();
  // On assert, the system can only recover with a reset.
#ifndef DEBUG
  NVIC_SystemReset();
#else
  app_error_save_and_stop(id, pc, info);
#endif // DEBUG
}

/**
 * @brief Function for application main entry.
 */
int main(void)
{
  // initialization functions
  log_init();
  application_timers_init();
  buttons_leds_init();
  
  NRF_LOG_INFO("Initialization successful\n");
  
  // ble related initialization
  ble_stack_init();
	gap_params_init();
	advertising_init();
	advertising_start();
  
  NRF_LOG_INFO("BLE initialization successful\n");
  
  // initialize ic2 devices
#ifdef MEASUREMENT_ACTIVE
  uint32_t err_code = hdc1000_init();
	APP_ERROR_CHECK(err_code);
  err_code = lps25hb_init();
	APP_ERROR_CHECK(err_code);
  
  NRF_LOG_INFO("i2c initialization successful\n");
#endif

  // start executions  
  application_timers_start();
  
  for (;;)
  {
    if (NRF_LOG_PROCESS() == false)
    {
      power_manage();
    }
  }  
}

