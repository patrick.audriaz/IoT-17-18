#include "ble_helpers.h"

// system
#include <string.h>

// BLE related
#include "ble.h"
#include "ble_advdata.h"
#include "ble_srv_common.h"
#include "ble_advertising.h"
#include "ble_hci.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"

// softdevice
#include "nrf_sdm.h"
#include "softdevice_handler.h"

// nrf sdk
#include "boards.h"
#include "nrf_log.h"
#include "app_timer.h"
#include "app_util_bds.h"
#include "fstorage.h"

// board support
#include "bsp.h"
#include "bsp_btn_ble.h"

// BLE related constants
#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */

#define CENTRAL_LINK_COUNT               0        /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1        /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define DEVICE_NAME                      "IoT"                               		/**< Name of device. Will be included in the advertising data. */

#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(100, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(200, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define APP_ADV_INTERVAL                 14400                                      /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms) --> 14400 = 9 seconds */
#define APP_ADV_TIMEOUT_IN_SECONDS       0                                          /**< The advertising timeout in units of seconds. */

#define APP_FEATURE_NOT_SUPPORTED        BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

/**< Weather Station Live Service UUID (same as environmental sensing service). */
#define BLE_UUID_WEATHER_STATION_LIVE_SERVICE  0x181A

/**< Handle of the current connection. */
static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;   

/**< Variables used for service data in advertising data. */
static uint8_t encodedBatteryServiceDataBuffer[1] = {0};
static uint8_t encodedWeatherStationServiceDataBuffer[6] = {0};

/**< Variables used for service uuids in advertising data. */
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_BATTERY_SERVICE,            BLE_UUID_TYPE_BLE},
                                   {BLE_UUID_WEATHER_STATION_LIVE_SERVICE,    BLE_UUID_TYPE_BLE}}; 

                                   
/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{ 
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
  bsp_btn_ble_on_ble_evt(p_ble_evt);
  on_ble_evt(p_ble_evt);
  ble_advertising_on_ble_evt(p_ble_evt);
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
  // Dispatch to the Advertising module last, since it will check if there are any
  // pending flash operations in fstorage. Let fstorage process system events first,
  // so that it can report correctly to the Advertising module.
  ble_advertising_on_sys_evt(sys_evt);
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
		
	// NRF_LOG_INFO("Device name is : %s \n", DEVICE_NAME);
	
  // Initialize the SoftDevice handler module.
  nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
  SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
  
  ble_enable_params_t ble_enable_params;
  uint32_t err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                           PERIPHERAL_LINK_COUNT,
                                                           &ble_enable_params);
  APP_ERROR_CHECK(err_code);

  // Check the ram settings against the used number of links
  CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);

  // Enable BLE stack.
#if (NRF_SD_BLE_API_VERSION == 3)
  ble_enable_params.gatt_enable_params.att_mtu = NRF_BLE_MAX_MTU_SIZE;
#endif
  err_code = softdevice_enable(&ble_enable_params);
  APP_ERROR_CHECK(err_code);

  // Register with the SoftDevice handler module for BLE events.
  err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
  APP_ERROR_CHECK(err_code);

  // Register with the SoftDevice handler module for system events.
  err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
  ble_gap_conn_sec_mode_t sec_mode;
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  uint32_t err_code = sd_ble_gap_device_name_set(&sec_mode,
                                                 (const uint8_t *)DEVICE_NAME,
                                                 strlen(DEVICE_NAME));
  APP_ERROR_CHECK(err_code);

  err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_THERMOMETER);
  APP_ERROR_CHECK(err_code);

  ble_gap_conn_params_t gap_conn_params;
  memset(&gap_conn_params, 0, sizeof(gap_conn_params));

  gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
  gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
  gap_conn_params.slave_latency     = SLAVE_LATENCY;
  gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

  err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
void sleep_mode_enter(void)
{
  uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
  APP_ERROR_CHECK(err_code);

  // Prepare wakeup buttons.
  err_code = bsp_btn_ble_sleep_mode_prepare();
  APP_ERROR_CHECK(err_code);

  // Go to system-off mode (this function will not return; wakeup will cause a reset).
  err_code = sd_power_system_off();
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
  switch (event)
  {
    case BSP_EVENT_SLEEP:
      sleep_mode_enter();
    break;

    case BSP_EVENT_DISCONNECT:
    {
      uint32_t err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      if (err_code != NRF_ERROR_INVALID_STATE)
      {
        APP_ERROR_CHECK(err_code);
      }
    }
    break;
        
    case BSP_EVENT_WHITELIST_OFF:
      if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
      {
        uint32_t err_code = ble_advertising_restart_without_whitelist();
        if (err_code != NRF_ERROR_INVALID_STATE)
        {
          APP_ERROR_CHECK(err_code);
        }
      }
      break;

    default:
      break;
  }
}

/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
void buttons_leds_init(void)
{
  uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                               APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                               bsp_event_handler);
  APP_ERROR_CHECK(err_code);

  bsp_event_t startup_event;
  err_code = bsp_btn_ble_init(NULL, &startup_event);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
  switch (ble_adv_evt)
  {
    case BLE_ADV_EVT_FAST:
    {
      NRF_LOG_INFO("Fast Advertising\r\n");
      uint32_t err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
      APP_ERROR_CHECK(err_code);
    }
    break;

    case BLE_ADV_EVT_IDLE:
      sleep_mode_enter();
    break;

    default:
      break;
  }
}

/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init(void)
{
  // Build advertising data struct to pass into @ref ble_advertising_init.
  ble_advdata_t advdata;
  memset(&advdata, 0, sizeof(advdata));

  advdata.name_type = BLE_ADVDATA_FULL_NAME;
  advdata.include_appearance = false;
  advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
  advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
  advdata.uuids_complete.p_uuids = m_adv_uuids;
  
  ble_adv_modes_config_t options = {0};
  options.ble_adv_fast_enabled  = true;
  options.ble_adv_fast_interval = APP_ADV_INTERVAL;
  options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

  uint32_t err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting advertising.
 */
void advertising_start(void)
{
  uint32_t err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising data.
 *
 * @details Encodes the required advertising data
 */
static void advdata_fill(ble_advdata_t* pAdvData, uint8_t battery_level, double dTemperature, double dHumidity, double dPressure)
{	
	static ble_advdata_service_data_t service_data[2] = { {0} , {0} };
   
	// fill service_data[0] with battery service data published in advertising data
	encodedBatteryServiceDataBuffer[0] = battery_level;
	service_data[0].service_uuid = BLE_UUID_BATTERY_SERVICE;
	service_data[0].data.size = sizeof(encodedBatteryServiceDataBuffer);
	service_data[0].data.p_data = encodedBatteryServiceDataBuffer;
	
	// fill service_data[1] with weather station service data published in advertising data
	int16_t temperature = (int16_t) (dTemperature * 100);
	uint16_t humidity = (uint16_t) (dHumidity * 100);
	uint16_t pressure = (uint16_t) (dPressure * 10);
	
	bds_int16_encode(&temperature, encodedWeatherStationServiceDataBuffer);
	bds_int16_encode((int16_t*) &humidity, encodedWeatherStationServiceDataBuffer + 2);
	bds_int16_encode((int16_t*) &pressure, encodedWeatherStationServiceDataBuffer + 4);
	
	service_data[1].service_uuid = BLE_UUID_WEATHER_STATION_LIVE_SERVICE;
	service_data[1].data.size = sizeof(encodedWeatherStationServiceDataBuffer);
	service_data[1].data.p_data = encodedWeatherStationServiceDataBuffer;
	
	// Build and set advertising data
	memset(pAdvData, 0, sizeof(*pAdvData));
  
	pAdvData->name_type = BLE_ADVDATA_FULL_NAME;
	pAdvData->include_appearance = false;
	pAdvData->flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
  pAdvData->uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
  pAdvData->uuids_complete.p_uuids = m_adv_uuids;
	pAdvData->service_data_count = sizeof(service_data) / sizeof(service_data[0]);
	pAdvData->p_service_data_array = service_data;
}

/**@brief Function for updating the advertising data (called by client upon measurement change).
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
void advdata_update(uint8_t battery_level, double dTemperature, double dHumidity, double dPressure)
{
	ble_advdata_t advdata;
	advdata_fill(&advdata, battery_level, dTemperature, dHumidity, dPressure);
		
	uint32_t err_code = ble_advdata_set(&advdata, NULL);
	APP_ERROR_CHECK(err_code);
}
