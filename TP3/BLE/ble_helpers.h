#ifndef _BLE_HELPERS_H_
#define _BLE_HELPERS_H_

#include <stdint.h>

#include "bsp.h"
/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void);
void gap_params_init(void);
void advertising_init(void);
void advertising_start(void);
void sleep_mode_enter(void);
void buttons_leds_init(void);
void advdata_update(uint8_t battery_level, double dTemperature, double dHumidity, double dPressure);

void bsp_event_handler(bsp_event_t event);

#endif
