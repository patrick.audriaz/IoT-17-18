#ifndef _HELPERS_H_
#define _HELPERS_H_

/**@brief Function for logging a measurement formatted as double as int.int
 */
void LogMeasurement(const char* szMsgStart, const char* szMsgEnd, double dMeasurementValue);

#endif
