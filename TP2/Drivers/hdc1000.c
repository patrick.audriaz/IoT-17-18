#include "hdc1000.h"
#include "i2c.h"
#include "helpers.h"

// nrf51
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_twi.h"
#include "app_error.h"
#include "nrf_log.h"

// global variables
static bool g_hdc1000_initialized = false;
static volatile bool g_data_ready = false;

// definitions
#define DATA_RDY_IN_PIN (15U)

#define HDC1000_ADDR                0x40

#define HDC1000_TEMP                0x00
#define HDC1000_HUMI                0x01
#define HDC1000_CONFIG              0x02

#define HDC1000_SERID_1             0xFB
#define HDC1000_SERID_2             0xFC
#define HDC1000_SERID_3             0xFD
#define HDC1000_MFID                0xFE
#define HDC1000_DEVID               0xFF

#define HDC1000_RST                 0x80
#define HDC1000_HEAT_ON             0x20
#define HDC1000_HEAT_OFF            0x00
#define HDC1000_BOTH_TEMP_HUMI      0x10
#define HDC1000_SINGLE_MEASUR       0x00
#define HDC1000_TEMP_HUMI_14BIT     0x00
#define HDC1000_TEMP_11BIT          0x04
#define HDC1000_HUMI_11BIT          0x01
#define HDC1000_HUMI_8BIT           0x02

// handler for data ready
static void data_ready_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
  NRF_LOG_INFO("data_ready_pin_handler\n");
  if (pin == DATA_RDY_IN_PIN)
  {
    if (action == NRF_GPIOTE_POLARITY_HITOLO)
    {
      g_data_ready = true;
    }
  }
}

ret_code_t hdc1000_init(void)
{
  if (g_hdc1000_initialized)
  {
    return NRF_SUCCESS;
  }

  // initialize i2c interface
  i2c_init();
  
  // probe for device
  ret_code_t err_code = i2c_probe(HDC1000_ADDR);
  APP_ERROR_CHECK(err_code);

  // configure gpio for data rdy pin
  if (! nrf_drv_gpiote_is_init())
  {
    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    in_config.pull = NRF_GPIO_PIN_PULLUP;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;

    err_code = nrf_drv_gpiote_in_init(DATA_RDY_IN_PIN, &in_config, data_ready_pin_handler);
    APP_ERROR_CHECK(err_code);
  }

  // modify global variable
  g_hdc1000_initialized = true;

  return NRF_SUCCESS;
}

ret_code_t hdc1000_read(uint8_t* pRxData, unsigned int rxDataLength)
{
  if (! g_hdc1000_initialized)
  {
    return MODULE_NOT_INITIALZED;
  }

  ret_code_t err_code = nrf_drv_twi_rx(&g_twi, HDC1000_ADDR, pRxData, rxDataLength);
  APP_ERROR_CHECK(err_code);

  return NRF_SUCCESS;
}

uint16_t hdc1000_read16(void)
{
  uint8_t rxData[2] = { 0 };

  ret_code_t err_code = hdc1000_read(rxData, sizeof(rxData));
  APP_ERROR_CHECK(err_code);
  
  uint16_t value = (((uint16_t) rxData[0]) << 8) + (uint16_t) rxData[1];
  NRF_LOG_DEBUG("Value read is %d (%d %d)\n", value, rxData[1], rxData[0]);

  return value;
}

ret_code_t hdc1000_setReadRegister(uint8_t reg)
{
  // reset data ready
  g_data_ready = false;

  // write register, device will thus read data and signal data ready
  uint8_t txData[1] = { reg };
  ret_code_t err_code = nrf_drv_twi_tx(&g_twi, HDC1000_ADDR, txData, sizeof(txData), true);
  APP_ERROR_CHECK(err_code);

  // wait for data ready
  uint16_t iteration = 0;
  while (! g_data_ready && (iteration++ < 20))
  {
    nrf_delay_ms(10);
  }

  return NRF_SUCCESS;
}

uint16_t hdc1000_getRawTemp(void)
{
  ret_code_t err_code = hdc1000_setReadRegister(HDC1000_TEMP);
  APP_ERROR_CHECK(err_code);

  return hdc1000_read16();
}

double hdc1000_getTemperature(void)
{
  double temp = hdc1000_getRawTemp();

  return (temp / 65536.0) * 165.0 - 40.0;
}

uint16_t hdc1000_getRawHumi(void)
{
  ret_code_t err_code = hdc1000_setReadRegister(HDC1000_HUMI);
  APP_ERROR_CHECK(err_code);

  return hdc1000_read16();
}

double hdc1000_getHumidity(void)
{
  double temp = hdc1000_getRawHumi();

  return (temp / 65536.0) * 100.0;
}


