#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "sdk_config.h"
#include "app_error.h"
#include "app_timer.h"
#include "sdk_errors.h"
#include "boards.h"

#include "nrf_drv_clock.h"
#include "nrf_log.h"
#include "nrf_error.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"

#include "i2c.h"
#include "nrf_drv_twi.h"
#include "hdc1000.h" //driver for the humidity and temperature sensors
#include "lps25hb.h" //driver for the pressure sensor
#include "helpers.h"

// definitions for timer values
#define APP_TIMER_PRESCALER              							0                                       	  														/**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          							4                                          															/**< Size of timer operation queues. */
#define LED_TIMER_INTERVAL_IN_MS                      1000                                       	 														/**< Value for the led timer in msecs. */
#define LED_TIMER_INTERVAL     					 							APP_TIMER_TICKS(LED_TIMER_INTERVAL_IN_MS, APP_TIMER_PRESCALER)					/**< Value for the led timer in number of ticks. */
#define MEASUREMENT_TIMER_INTERVAL_IN_MS              20000                                       														/**< Value for the measurement timer in msecs (= 20 sec). */
#define MEASUREMENT_TIMER_INTERVAL			 							APP_TIMER_TICKS(MEASUREMENT_TIMER_INTERVAL_IN_MS, APP_TIMER_PRESCALER)	/**< Value for the measurement timer in number of ticks (= 1 sec). */
#define DELAY_LED																			1000																																		/**< Value for the toggle led.  */

// App timers definition
APP_TIMER_DEF(g_led_timer_id);  				/**< Definition of the timer used for toggling the leds */
APP_TIMER_DEF(g_measurement_timer_id);  /**< Definition of the measurement used for the clock */

// variable used for storing the led list
const uint8_t leds_list[LEDS_NUMBER] = LEDS_LIST;

// variable used for switching leds
uint32_t g_index = 0;

// variable times
uint32_t times = 0;

//variable for storing the measurement averages
uint32_t avgTemp = 0;
uint32_t avgPres = 0;
uint32_t avgHum = 0;

//varibale used to count the # of mesures done
uint32_t turn = 1; 

/**@brief Function for initializing the nrf log module.
 */
 
static uint32_t getTime(){
	nrf_delay_ms(DELAY_LED);
	return times+=(DELAY_LED/LED_TIMER_INTERVAL_IN_MS);
}

static void log_init(void)
{
  ret_code_t err_code = NRF_LOG_INIT(getTime);
	APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling led toggling.
 *
 * @details This function will be called every LED_TIMER_INTERVAL
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler (not used in this case).
 */
static void led_timer_handler(void * p_context)
{	
  LEDS_INVERT(1 << leds_list[g_index]);

  g_index = (g_index + 1) % LEDS_NUMBER;
	
  //NRF_LOG_INFO("Inverting led %d\n", g_index);
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates application timers.
 */
static void measurement_timer_handler(void * p_context)
{		
  double dCurrentTemperature = hdc1000_getTemperature();
	avgTemp = avgTemp + dCurrentTemperature;
  LogMeasurement("Current temperature is", "degrees", dCurrentTemperature);
	
  double dCurrentHumidity = hdc1000_getHumidity();
	avgHum = avgHum + dCurrentHumidity;
  LogMeasurement("Current humidity is", "%", dCurrentHumidity);
  
  double dCurrentPressure = lps25hb_getPressure();
	avgPres = avgPres + dCurrentPressure;
  LogMeasurement("Current pressure is", "hPa (mbar)", dCurrentPressure);
	
	NRF_LOG_INFO("AVERAGE temperature is %d degrees\n", avgTemp/turn);
	NRF_LOG_INFO("AVERAGE humidity is %d \%\n", avgHum/turn);
	NRF_LOG_INFO("AVERAGE pressure is %d hPa (mbar)\n", avgPres/turn);
	NRF_LOG_INFO("Uptime %d seconds\n", turn*(MEASUREMENT_TIMER_INTERVAL_IN_MS/1000));
	
	turn++;
}

static void init_timers(void)
{
  // Initialize the timer module
  APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

  // Create the timers
  uint32_t err_code = app_timer_create(&g_led_timer_id, APP_TIMER_MODE_REPEATED, led_timer_handler);
  APP_ERROR_CHECK(err_code);
	
  err_code = app_timer_create(&g_measurement_timer_id, APP_TIMER_MODE_REPEATED, measurement_timer_handler);
  APP_ERROR_CHECK(err_code);
	
}

/**@brief Function for starting timers.
*/
static void start_timers(void)
{
  uint32_t err_code = app_timer_start(g_led_timer_id, LED_TIMER_INTERVAL, NULL);
  APP_ERROR_CHECK(err_code); 

	err_code = app_timer_start(g_measurement_timer_id, MEASUREMENT_TIMER_INTERVAL, NULL);
  APP_ERROR_CHECK(err_code);	
}

// Function starting the internal LFCLK oscillator.
// This is needed by RTC1 which is used by the application timer
// (When SoftDevice is enabled the LFCLK is always running and this is not needed).
static void lfclk_request(void)
{
  uint32_t err_code = nrf_drv_clock_init();
  APP_ERROR_CHECK(err_code);

  nrf_drv_clock_lfclk_request(NULL);
}

/**@brief Function for the Power Management.
 */
static void power_manage(void)
{
  // Use directly __WFE and __SEV macros since the SoftDevice is not available.

  // Wait for event.
  __WFE();

  // Clear Event Register.
	__SEV();
  __WFE();
}

/**
 * @brief Function for application main entry.
 */
int main(void)
{
  // initialization functions
  lfclk_request();
  log_init();
	NRF_LOG_INFO("Initialization successful\n");
  init_timers();
	NRF_LOG_INFO("Timer initialization successful\n");
	hdc1000_init(); 																		 
	NRF_LOG_INFO("HDC1000 initialization successful\n");
  lps25hb_init();
	NRF_LOG_INFO("lps25hb initialization successful\n");
  start_timers();
		
  // Configure LED-pins as outputs
  LEDS_CONFIGURE(LEDS_MASK);

  for (;;)
  {
    power_manage();
  }  
}