#ifndef _I2C_H_
#define _I2C_H_

#include <stdint.h>

// twi driver
#include "nrf_drv_twi.h"

// TWI instance
extern const nrf_drv_twi_t g_twi;

void i2c_init(void);
ret_code_t i2c_probe(uint8_t chip_id);

#endif
