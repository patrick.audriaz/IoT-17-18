#ifndef HDC1000_H
#define HDC1000_H

#include "sdk_errors.h"

ret_code_t hdc1000_init(void);
double hdc1000_getTemperature(void);
double hdc1000_getHumidity(void);

#endif
