#include "helpers.h"

// nrf sdk
#include "nrf_log.h"

/**@brief Function for formatting a double as int.int
 */
void FormatDouble(double dValue, int* pIntegerPart, int* pFractionalPart)
{
  *pIntegerPart = (int) dValue;
  *pFractionalPart = 0;
  if (dValue > 0.0)
  {
    *pFractionalPart = ((int) ((dValue - *pIntegerPart) * 1000));
  }
  else
  {
    *pFractionalPart = ((int) ((-dValue + *pIntegerPart) * 1000));
  }
}

/**@brief Function for logging a measurement formatted as double
 */
void LogMeasurement(const char* szMsgStart, const char* szMsgEnd, double dMeasurementValue)
{
  // format the measurement as int.int
  int measurement_int = 0;
  int measurement_frac = 0;
  FormatDouble(dMeasurementValue, &measurement_int, &measurement_frac);
  NRF_LOG_INFO("%s: %d.%03d %s\n", (uint32_t) szMsgStart, measurement_int, measurement_frac, (uint32_t) szMsgEnd);
}
