#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <stdint.h>

// structure used for storing weather station data measurement
typedef struct
{
	int16_t temperature;
	uint16_t humidity;
	uint16_t pressure;
	uint16_t time;
} measurement_t;

#endif
