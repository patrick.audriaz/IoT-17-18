// system
#include <stdio.h>

// for updating the GATT database
#include "ble_helpers.h"

// nRF SDK
#include "nrf_log.h"

// for sensors
#include "hdc1000.h"
#include "lps25hb.h"
#include "battery.h"
#include "helpers.h"

#include "measurement_history.h"

// definitions of data used for storing measurements
#define MEMORY_SIZE 10000
#define M MEMORY_SIZE/sizeof(measurement_t) 

//structure de donn�es permettant de stocket les infos
//allocation du vecteur measurementArray de mani�re statique 
measurement_t measurementArray[M] = {0};
uint16_t measurementIndex = 0;

// implementation of functions
uint16_t getLastMeasurementTime(void){ 
	return measurementArray[measurementIndex % M].time;
}
  
uint16_t getMeasurementArraySize(void){
  return sizeof(measurementArray);
}

bool getMeasurement(uint16_t measurement_index, measurement_t* pMeasurement){
  	if(measurementIndex < measurement_index){
		return false;
	}
	if(measurementIndex >  measurement_index - M){
		return false;
	}
	*pMeasurement = measurementArray[measurement_index % M];
	return true;
}

//fonction permettant de stocker une nouvelle mesure dans un vecteur circulaire
void addMeasurement(measurement_t* pMeasurement){
	// stock derni�res mesures
	measurementArray[measurementIndex % M].humidity 	= pMeasurement->humidity;
	measurementArray[measurementIndex % M].pressure   	= pMeasurement->pressure;
	measurementArray[measurementIndex % M].temperature = pMeasurement->temperature;
	
	// stock heure derni�re mesure
	measurementArray[measurementIndex % M].time = pMeasurement->time;
	
	measurementIndex++;
	
	// set the number of entries in the GATT database
	ble_update_gatt_database_and_notify(M, measurementIndex, pMeasurement);
  
  NRF_LOG_INFO("Updating gatt database with new measurement at index %d and time %d\n", measurementIndex, getLastMeasurementTime());
}
