#ifndef MEASUREMENT_HISTORY_H
#define MEASUREMENT_HISTORY_H

#include <stdbool.h>

#include "measurement.h"

// function prototypes used for storing and accessing measurements
uint16_t getLastMeasurementTime(void);
uint16_t getMeasurementArraySize(void);
bool getMeasurement(uint16_t measurement_index, measurement_t* pMeasurement);
void addMeasurement(measurement_t* pMeasurement);

#endif
