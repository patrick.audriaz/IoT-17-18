#ifndef BATTERY_H
#define BATTERY_H

#include <stdint.h>

uint8_t get_battery_level(void);

#endif
