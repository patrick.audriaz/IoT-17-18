#ifndef LPS25HB_H
#define LPS25HB_H

#include "sdk_errors.h"

ret_code_t lps25hb_init(void);
double lps25hb_getPressure(void);

#endif
