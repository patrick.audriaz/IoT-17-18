#include "battery.h"

// nRF51 sdk
#include "nrf_soc.h"

#define VBAT_MAX_IN_MV 3300

uint8_t get_battery_level(void)
{
	// Configure ADC
	NRF_ADC->CONFIG = (ADC_CONFIG_RES_8bit                        << ADC_CONFIG_RES_Pos) |
                    (ADC_CONFIG_INPSEL_SupplyOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) |
                    (ADC_CONFIG_REFSEL_VBG                      << ADC_CONFIG_REFSEL_Pos) |
                    (ADC_CONFIG_PSEL_Disabled                   << ADC_CONFIG_PSEL_Pos) |
                  	(ADC_CONFIG_EXTREFSEL_None                  << ADC_CONFIG_EXTREFSEL_Pos);

	NRF_ADC->EVENTS_END = 0;
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;

	NRF_ADC->EVENTS_END = 0;    // Stop any running conversions.
	NRF_ADC->TASKS_START = 1;

	while (!NRF_ADC->EVENTS_END)
  {
	}

	uint16_t vbg_in_mv = 1200;
	uint8_t adc_max = 255;
	uint16_t vbat_current_in_mv = (NRF_ADC->RESULT * 3 * vbg_in_mv) / adc_max;
	
	NRF_ADC->EVENTS_END     = 0;
	NRF_ADC->TASKS_STOP     = 1;

	return (uint8_t) ((vbat_current_in_mv * 100) / VBAT_MAX_IN_MV);
}
