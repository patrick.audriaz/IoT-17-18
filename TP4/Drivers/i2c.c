#include "i2c.h"

// for sdk configuration
#include "sdk_config.h"

// twi driver
#include "nrf_drv_twi.h"

// nrf libraries
#include "app_util_platform.h"
#include "nrf_log.h"

// SCL and SDA pins
#define TWI_MASTER_CONFIG_CLOCK_PIN_NUMBER (6U)
#define TWI_MASTER_CONFIG_DATA_PIN_NUMBER (5U)

// global variables
static bool g_i2c_initialized = false;
// TWI instance
const nrf_drv_twi_t g_twi = NRF_DRV_TWI_INSTANCE(0);

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
  switch (p_event->type)
  {
    case NRF_DRV_TWI_EVT_DONE:
      break;

    default:
      break;
  }
}

void i2c_init(void)
{
  if (g_i2c_initialized)
  {
    return;
  }
  const nrf_drv_twi_config_t twi_config = {
        .scl                = TWI_MASTER_CONFIG_CLOCK_PIN_NUMBER,
        .sda                = TWI_MASTER_CONFIG_DATA_PIN_NUMBER,
        .frequency          = NRF_TWI_FREQ_100K,
        .interrupt_priority = APP_IRQ_PRIORITY_LOW
      };
  
  ret_code_t err_code = nrf_drv_twi_init(&g_twi, &twi_config, /*twi_handler*/ NULL, NULL);
  APP_ERROR_CHECK(err_code);

  nrf_drv_twi_enable(&g_twi);

  NRF_LOG_INFO("i2c_init succeeded\n");

  // modify global variable
  g_i2c_initialized = true;
}

ret_code_t i2c_probe(uint8_t chip_id)
{  
  uint8_t rx_data = 0;
  ret_code_t err_code = nrf_drv_twi_rx(&g_twi, chip_id, (uint8_t*) &rx_data, sizeof(rx_data));
  if (err_code == NRF_SUCCESS)
  {
    NRF_LOG_INFO("device with id 0x%x detected (read)\n", chip_id);
    return NRF_SUCCESS;
  }
  
  uint8_t tx_data = 0;
  err_code = nrf_drv_twi_tx(&g_twi, chip_id, &tx_data, sizeof(tx_data), false);
  if (err_code == NRF_SUCCESS)
  {
    NRF_LOG_INFO("device with id 0x%x detected (write)\n", chip_id);
    return NRF_SUCCESS;
  }


  NRF_LOG_INFO("Device with id 0x%x not detected\n", chip_id);

  return NRF_ERROR_NOT_FOUND;
}
