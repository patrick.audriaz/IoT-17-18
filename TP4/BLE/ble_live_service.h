/* This file was generated by plugin 'Nordic Semiconductor nRF5x v.1.2.2' (BDS version 1.1.3135.0) */

#ifndef BLE_LIVE_SERVICE_H__
#define BLE_LIVE_SERVICE_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "app_util_bds.h"



/**@brief Live Service event type. */
typedef enum
{ 
    BLE_LIVE_SERVICE_MEASUREMENT_EVT_NOTIFICATION_ENABLED,  /**< Measurement value notification enabled event. */
    BLE_LIVE_SERVICE_MEASUREMENT_EVT_NOTIFICATION_DISABLED, /**< Measurement value notification disabled event. */
} ble_live_service_evt_type_t;

// Forward declaration of the ble_live_service_t type.
typedef struct ble_live_service_s ble_live_service_t;








/**@brief Measurement structure. */
typedef struct
{
    uint16_t time;
    uint16_t temperature;
    uint16_t humidity;
    uint16_t pressure;
} ble_live_service_measurement_t;

/**@brief Live Service Service event. */
typedef struct
{
    ble_live_service_evt_type_t evt_type;    /**< Type of event. */
    union {
        uint16_t cccd_value; /**< Holds decoded data in Notify and Indicate event handler. */
    } params;
} ble_live_service_evt_t;

/**@brief Live Service Service event handler type. */
typedef void (*ble_live_service_evt_handler_t) (ble_live_service_t * p_live_service, ble_live_service_evt_t * p_evt);

/**@brief Live Service Service init structure. This contains all options and data needed for initialization of the service */
typedef struct
{
    ble_live_service_evt_handler_t     evt_handler; /**< Event handler to be called for handling events in the Live Service Service. */
    ble_live_service_measurement_t ble_live_service_measurement_initial_value; /**< If not NULL, initial value of the Measurement characteristic. */ 
} ble_live_service_init_t;

/**@brief Live Service Service structure. This contains various status information for the service.*/
struct ble_live_service_s
{
    ble_live_service_evt_handler_t evt_handler; /**< Event handler to be called for handling events in the Live Service Service. */
    uint16_t service_handle; /**< Handle of Live Service Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t measurement_handles; /**< Handles related to the Measurement characteristic. */
    uint16_t conn_handle; /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
};

/**@brief Function for initializing the Live Service.
 *
 * @param[out]  p_live_service       Live Service Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_live_service_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_live_service_init(ble_live_service_t * p_live_service, const ble_live_service_init_t * p_live_service_init);

/**@brief Function for handling the Application's BLE Stack events.*/
void ble_live_service_on_ble_evt(ble_live_service_t * p_live_service, ble_evt_t * p_ble_evt);

/**@brief Function for setting the Measurement.
 *
 * @details Sets a new value of the Measurement characteristic. The new value will be sent
 *          to the client the next time the client reads the Measurement characteristic.
 *          This function is only generated if the characteristic's Read property is not 'Excluded'.
 *
 * @param[in]   p_live_service                 Live Service Service structure.
 * @param[in]   p_measurement  New Measurement.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
uint32_t ble_live_service_measurement_set(ble_live_service_t * p_live_service, ble_live_service_measurement_t * p_measurement);

#endif //_BLE_LIVE_SERVICE_H__
