#include "ble_helpers.h"

// system
#include <string.h>

// BLE related
#include "ble.h"
#include "ble_advdata.h"
#include "ble_srv_common.h"
#include "ble_advertising.h"
#include "ble_hci.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"

// BLE services
#include "ble_live_service.h"
#include "ble_history_service.h"

// softdevice
#include "nrf_sdm.h"
#include "softdevice_handler.h"

// nrf sdk
#include "boards.h"
#include "nrf_log.h"
#include "app_timer.h"
#include "app_util_bds.h"
#include "fstorage.h"

// board support
#include "bsp.h"
#include "bsp_btn_ble.h"

// for measurement history
#include "measurement_history.h"

// BLE related constants
#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */

#define CENTRAL_LINK_COUNT               0        /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1        /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define DEVICE_NAME                      "IoT"                               /**< Name of device. Will be included in the advertising data. */

#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(100, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(200, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define APP_ADV_INTERVAL                 300                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       0                                          /**< The advertising timeout in units of seconds. */

#define APP_FEATURE_NOT_SUPPORTED        BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define FIRST_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY    APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT     3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

/**< Weather Station Live Service UUID (same as environmental sensing service). */
#define BLE_UUID_WEATHER_STATION_LIVE_SERVICE  0x1830
#define BLE_UUID_WEATHER_STATION_HISTORY_SERVICE  0x1831

/**< Handle of the current connection. */
static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;   

/**< Variables used for service data in advertising data. */
static uint8_t encodedBatteryServiceDataBuffer[1] = {0};
static uint8_t encodedWeatherStationServiceDataBuffer[6] = {0};

/**< Variables used for service uuids in advertising data. */
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_BATTERY_SERVICE,            BLE_UUID_TYPE_BLE},
                                   {BLE_UUID_WEATHER_STATION_LIVE_SERVICE,    BLE_UUID_TYPE_BLE},
                                   {BLE_UUID_WEATHER_STATION_HISTORY_SERVICE,    BLE_UUID_TYPE_BLE}}; 
       
/**< Service instances. */                                   
ble_history_service_t m_history_service; 
ble_live_service_t m_live_service;                           
                                    
/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{ 
  switch (p_ble_evt->header.evt_id)
  {
    case BLE_GAP_EVT_CONNECTED:
    {
      NRF_LOG_INFO("Connected\r\n");
      uint32_t err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
      APP_ERROR_CHECK(err_code);
      m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    }
    break; // BLE_GAP_EVT_CONNECTED

    case BLE_GAP_EVT_DISCONNECTED:
    {
      NRF_LOG_INFO("Disconnected\r\n");
      m_conn_handle = BLE_CONN_HANDLE_INVALID;
    }
    break; // BLE_GAP_EVT_DISCONNECTED
    
    case BLE_GATTC_EVT_TIMEOUT:
    {
      // Disconnect on GATT Client timeout event.
      NRF_LOG_DEBUG("GATT Client Timeout.\r\n");
      uint32_t err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                                BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      APP_ERROR_CHECK(err_code);
    }
    break; // BLE_GATTC_EVT_TIMEOUT

    case BLE_GATTS_EVT_TIMEOUT:
    {
      // Disconnect on GATT Server timeout event.
      NRF_LOG_DEBUG("GATT Server Timeout.\r\n");
      uint32_t err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                                BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      APP_ERROR_CHECK(err_code);
    }
    break; // BLE_GATTS_EVT_TIMEOUT
    
    default:
      // No implementation needed.
    break;
  }
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
  bsp_btn_ble_on_ble_evt(p_ble_evt);
  on_ble_evt(p_ble_evt);    
  ble_history_service_on_ble_evt(&m_history_service, p_ble_evt); 
  ble_live_service_on_ble_evt(&m_live_service, p_ble_evt);
  ble_advertising_on_ble_evt(p_ble_evt);
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
  // Dispatch to the Advertising module last, since it will check if there are any
  // pending flash operations in fstorage. Let fstorage process system events first,
  // so that it can report correctly to the Advertising module.
  ble_advertising_on_sys_evt(sys_evt);
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
  // Initialize the SoftDevice handler module.
  nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
  SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
  
  ble_enable_params_t ble_enable_params;
  uint32_t err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                           PERIPHERAL_LINK_COUNT,
                                                           &ble_enable_params);
  APP_ERROR_CHECK(err_code);

  // Check the ram settings against the used number of links
  CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);

  // Enable BLE stack.
#if (NRF_SD_BLE_API_VERSION == 3)
  ble_enable_params.gatt_enable_params.att_mtu = NRF_BLE_MAX_MTU_SIZE;
#endif
  err_code = softdevice_enable(&ble_enable_params);
  APP_ERROR_CHECK(err_code);

  // Register with the SoftDevice handler module for BLE events.
  err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
  APP_ERROR_CHECK(err_code);

  // Register with the SoftDevice handler module for system events.
  err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
  ble_gap_conn_sec_mode_t sec_mode;
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

  uint32_t err_code = sd_ble_gap_device_name_set(&sec_mode,
                                                 (const uint8_t *)DEVICE_NAME,
                                                 strlen(DEVICE_NAME));
  APP_ERROR_CHECK(err_code);

  err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_THERMOMETER);
  APP_ERROR_CHECK(err_code);

  ble_gap_conn_params_t gap_conn_params;
  memset(&gap_conn_params, 0, sizeof(gap_conn_params));

  gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
  gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
  gap_conn_params.slave_latency     = SLAVE_LATENCY;
  gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

  err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
void sleep_mode_enter(void)
{
  uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
  APP_ERROR_CHECK(err_code);

  // Prepare wakeup buttons.
  err_code = bsp_btn_ble_sleep_mode_prepare();
  APP_ERROR_CHECK(err_code);

  // Go to system-off mode (this function will not return; wakeup will cause a reset).
  err_code = sd_power_system_off();
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
  switch (event)
  {
    case BSP_EVENT_SLEEP:
      sleep_mode_enter();
    break;

    case BSP_EVENT_DISCONNECT:
    {
      uint32_t err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
      if (err_code != NRF_ERROR_INVALID_STATE)
      {
        APP_ERROR_CHECK(err_code);
      }
    }
    break;
        
    case BSP_EVENT_WHITELIST_OFF:
      if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
      {
        uint32_t err_code = ble_advertising_restart_without_whitelist();
        if (err_code != NRF_ERROR_INVALID_STATE)
        {
          APP_ERROR_CHECK(err_code);
        }
      }
      break;

    default:
      break;
  }
}

/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
void buttons_leds_init(void)
{
  uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS,
                               APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
                               bsp_event_handler);
  APP_ERROR_CHECK(err_code);

  bsp_event_t startup_event;
  err_code = bsp_btn_ble_init(NULL, &startup_event);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
  switch (ble_adv_evt)
  {
    case BLE_ADV_EVT_FAST:
    {
      NRF_LOG_INFO("Fast Advertising\r\n");
      uint32_t err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
      APP_ERROR_CHECK(err_code);
    }
    break;

    case BLE_ADV_EVT_IDLE:
      sleep_mode_enter();
    break;
    
    default:
      break;
  }
}

/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init(void)
{
  // Build advertising data struct to pass into @ref ble_advertising_init.
  ble_advdata_t advdata;
  memset(&advdata, 0, sizeof(advdata));

  advdata.name_type = BLE_ADVDATA_FULL_NAME;
  advdata.include_appearance = false;
  advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
  advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
  advdata.uuids_complete.p_uuids = m_adv_uuids;
  
  ble_adv_modes_config_t options = {0};
  options.ble_adv_fast_enabled  = true;
  options.ble_adv_fast_interval = APP_ADV_INTERVAL;
  options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

  uint32_t err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting advertising.
 */
void advertising_start(void)
{
  uint32_t err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising data.
 *
 * @details Encodes the required advertising data
 */
static void advdata_fill(ble_advdata_t* pAdvData, uint8_t battery_level, double dTemperature, double dHumidity, double dPressure)
{	
	static ble_advdata_service_data_t service_data[2] = { {0} , {0} };
   
	// fill service_data[0] with battery service data published in advertising data
	encodedBatteryServiceDataBuffer[0] = battery_level;
	service_data[0].service_uuid = BLE_UUID_BATTERY_SERVICE;
	service_data[0].data.size = sizeof(encodedBatteryServiceDataBuffer);
	service_data[0].data.p_data = encodedBatteryServiceDataBuffer;
	
	// fill service_data[1] with weather station service data published in advertising data
	int16_t temperature = (int16_t) (dTemperature * 100);
	uint16_t humidity = (uint16_t) (dHumidity * 100);
	uint16_t pressure = (uint16_t) (dPressure * 10);
	
	bds_int16_encode(&temperature, encodedWeatherStationServiceDataBuffer);
	bds_int16_encode((int16_t*) &humidity, encodedWeatherStationServiceDataBuffer + 2);
	bds_int16_encode((int16_t*) &pressure, encodedWeatherStationServiceDataBuffer + 4);
	
	service_data[1].service_uuid = BLE_UUID_WEATHER_STATION_LIVE_SERVICE;
	service_data[1].data.size = sizeof(encodedWeatherStationServiceDataBuffer);
	service_data[1].data.p_data = encodedWeatherStationServiceDataBuffer;
	
	// Build and set advertising data
	memset(pAdvData, 0, sizeof(*pAdvData));
  
	pAdvData->name_type = BLE_ADVDATA_FULL_NAME;
	pAdvData->include_appearance = false;
	pAdvData->flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
  pAdvData->uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
  pAdvData->uuids_complete.p_uuids = m_adv_uuids;
	pAdvData->service_data_count = sizeof(service_data) / sizeof(service_data[0]);
	pAdvData->p_service_data_array = service_data;
}

/**@brief Function for updating the advertising data (called by client upon measurement change).
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
void advdata_update(uint8_t battery_level, double dTemperature, double dHumidity, double dPressure)
{
	ble_advdata_t advdata;
	advdata_fill(&advdata, battery_level, dTemperature, dHumidity, dPressure);
		
	uint32_t err_code = ble_advdata_set(&advdata, NULL);
	APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
  NRF_LOG_INFO("Connection event: %d\n", p_evt->evt_type);
  if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
  {
    uint32_t err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
    APP_ERROR_CHECK(err_code);
  }
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
  APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
void conn_params_init(void)
{
  ble_conn_params_init_t cp_init;
  memset(&cp_init, 0, sizeof(cp_init));

  cp_init.p_conn_params                  = NULL;
  cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
  cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
  cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
  cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
  cp_init.disconnect_on_fail             = false;
  cp_init.evt_handler                    = on_conn_params_evt;
  cp_init.error_handler                  = conn_params_error_handler;
  
  uint32_t err_code = ble_conn_params_init(&cp_init);
  APP_ERROR_CHECK(err_code);
}

/**@brief Function for updating the GATT database
 */
void ble_update_gatt_database(uint16_t nbrOfEntries, uint16_t nbrOfMeasurements, measurement_t* pMeasurement)
{
  // update the history service characteristics
  ble_history_service_nbrofentries_t service_nbrOfEntries;
  service_nbrOfEntries.value = nbrOfEntries;
  uint32_t err_code = ble_history_service_nbrofentries_set(&m_history_service, &service_nbrOfEntries);
  APP_ERROR_CHECK(err_code);
  
  ble_history_service_nbrofmeasurements_t service_nbrOfMeasurements;
  service_nbrOfMeasurements.value = nbrOfMeasurements;
  err_code = ble_history_service_nbrofmeasurements_set(&m_history_service, &service_nbrOfMeasurements);
  APP_ERROR_CHECK(err_code);
  
  // update the live service characteristics
  ble_live_service_measurement_t service_measurement = {0};
  service_measurement.temperature = pMeasurement->temperature;
  service_measurement.humidity = pMeasurement->humidity;
  service_measurement.pressure = pMeasurement->pressure;
  service_measurement.time = pMeasurement->time;
          
  err_code = ble_live_service_measurement_set(&m_live_service, &service_measurement);
  APP_ERROR_CHECK(err_code);  
}

/**@brief Function for handling the History service events.
 *
 * @details This function will be called for all History service events which are passed to
 *          the application.
 *
 * @param[in]   p_history_service   History service structure.
 * @param[in]   p_evt   Event received from the History service.
 */
static void on_history_service_evt(ble_history_service_t * p_history_service, ble_history_service_evt_t * p_evt)
{
  switch (p_evt->evt_type)
  { 
    case BLE_HISTORY_SERVICE_READINDEX_EVT_WRITE:
    {          
      // SECTION TO BE MODIFIED 
      NRF_LOG_INFO("[Bluetooth_IF]: HISTORY_SERVICE_READINDEX evt WRITE with value %d\r\n", p_evt->params.readindex.value);

      // set the measurement value corresponding to the index in the GATT database          
			
			// first read the measurement
			// second call ble_history_service_measurement_set() for setting the correct value
			// in the characteristic
      measurement_t measurement = {0};
      if (getMeasurement(p_evt->params.readindex.value, &measurement))
      {
        NRF_LOG_INFO("Got measurement at index %d\r\n", p_evt->params.readindex.value);

        // initialiaze the data for the characteristic				
        ble_history_service_measurement_t data = {0};
				data.temperature = measurement.temperature;
				data.pressure = measurement.pressure;
				data.humidity = measurement.humidity;				
        
        NRF_LOG_INFO("Read index updated to new value %d -> setting measurement value in GATT database to time: %d",
                     p_evt->params.readindex.value,
                     measurement.time);
        NRF_LOG_INFO("temperature: %d.%d degrees, humidity %d.%d %, pressure: %d hPa\n",
                     measurement.temperature, measurement.temperature % 100,
                     measurement.humidity, measurement.humidity % 100,
                     measurement.pressure);
				
				// call ble_history_service_measurement_set() for setting the characteristic
				ble_history_service_measurement_set(p_history_service, &data);
        
      }
      // SECTION TO BE MODIFIED
    }            
    break; 
 
    default:
      // No implementation needed.
    break;
  }
}

/**@brief Function for handling the Live Service events.
 *
 * @details This function will be called for all Live Service events which are passed to
 *          the application.
 *
 * @param[in]   p_live_service   Live Service structure.
 * @param[in]   p_evt   Event received from the Live Service.
 */
static void on_live_service_evt(ble_live_service_t * p_live_service, ble_live_service_evt_t * p_evt)
{
  switch (p_evt->evt_type)
  { 
    default:
    {
      // No implementation needed.
    }
    break;
  }
}

/**@brief Function for initializing the Services generated by Bluetooth Developer Studio.
 *
 *
 * @return      NRF_SUCCESS on successful initialization of services, otherwise an error code.
 */
void services_init(void)
{
    uint32_t    err_code; 
    
  // Initialize History service.
  ble_history_service_init_t    history_service_init; 
  memset(&history_service_init, 0, sizeof(history_service_init));

  history_service_init.evt_handler = on_history_service_evt; 
  memset(&history_service_init.ble_history_service_nbrofentries_initial_value.value,
         0x00,
         sizeof(history_service_init.ble_history_service_nbrofentries_initial_value.value));
  memset(&history_service_init.ble_history_service_nbrofmeasurements_initial_value.value,
         0x00,
         sizeof(history_service_init.ble_history_service_nbrofmeasurements_initial_value.value));
  memset(&history_service_init.ble_history_service_readindex_initial_value.value,
         0x00,
         sizeof(history_service_init.ble_history_service_readindex_initial_value.value));
  memset(&history_service_init.ble_history_service_measurement_initial_value.time,
         0x00,
         sizeof(history_service_init.ble_history_service_measurement_initial_value.time));
  memset(&history_service_init.ble_history_service_measurement_initial_value.temperature,
         0x00,
         sizeof(history_service_init.ble_history_service_measurement_initial_value.temperature));
  memset(&history_service_init.ble_history_service_measurement_initial_value.humidity,
         0x00,
         sizeof(history_service_init.ble_history_service_measurement_initial_value.humidity));
  memset(&history_service_init.ble_history_service_measurement_initial_value.pressure,
         0x00,
         sizeof(history_service_init.ble_history_service_measurement_initial_value.pressure));

  err_code = ble_history_service_init(&m_history_service, &history_service_init);
  APP_ERROR_CHECK(err_code);

  // Initialize Live Service.
  ble_live_service_init_t    live_service_init; 
  memset(&live_service_init, 0, sizeof(live_service_init));

  live_service_init.evt_handler = on_live_service_evt; 
  memset(&live_service_init.ble_live_service_measurement_initial_value.time,
         0x00,
         sizeof(live_service_init.ble_live_service_measurement_initial_value.time));
  memset(&live_service_init.ble_live_service_measurement_initial_value.temperature,
         0x00,
         sizeof(live_service_init.ble_live_service_measurement_initial_value.temperature));
  memset(&live_service_init.ble_live_service_measurement_initial_value.humidity,
         0x00,
         sizeof(live_service_init.ble_live_service_measurement_initial_value.humidity));
  memset(&live_service_init.ble_live_service_measurement_initial_value.pressure,
         0x00,
         sizeof(live_service_init.ble_live_service_measurement_initial_value.pressure));

  err_code = ble_live_service_init(&m_live_service, &live_service_init);
  APP_ERROR_CHECK(err_code);
}


